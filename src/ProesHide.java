import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import java.awt.*;

@ScriptManifest(name = "ProesHide", author = "Proeskoet", version = 1.0, info = "Beta", logo = "")


public class ProesHide extends Script {

    //Areas
    Area cowArea = new Area(0,0,0,0);

    Boolean pickup = false;

    @Override
    public void onStart(){

    }

    public enum State {
        WALK, BANK, PICKUP, WAIT, ATTACK
    }

    private State getState(){

        //inventory is full
        if(getInventory().isFull()){
            return State.BANK;
        }

        //inventory empty, player not in area
        if(!cowArea.contains(myPosition())){
            return State.WALK;
        }


        if(pickup){
            return State.PICKUP;
        }

        if(!getInventory().isFull() && myPlayer().getHealthPercentCache() > 50){
            return State.ATTACK;
        }

        return State.WAIT;
    }

    @Override
    public void onExit(){

    }

    @Override
    public void onPaint(Graphics2D g){}

    @Override
    public int onLoop(){
        State currentState = getState();

        if(currentState == State.ATTACK){

        }

        if(currentState == State.BANK){

        }

        if(currentState == State.PICKUP){

        }

        if(currentState == State.WALK){

        }
        return random(500, 5000);
    }



}
