import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.RS2Widget;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;
import tools.Timing;

import java.awt.*;

@ScriptManifest(name = "ProesClay", author = "Proeskoet", version = 1.0, info = "Beta", logo = "")


public class ProesClay extends Script {

    private long clayCount = 1;
    private long softCount = 0;

    private long startTime;
    private long runTime;

    private int clayToMine = 100;

    Area clayArea = new Area(3236, 3437, 3241, 3432);
    Area mineArea = new Area(
            new int[][]{
                    { 3179, 3380 },
                    { 3169, 3368 },
                    { 3171, 3363 },
                    { 3179, 3361 },
                    { 3188, 3376 },
                    { 3185, 3380 }
            }
    );

    boolean mine = false;

    @Override
    public void onStart() {
        startTime = System.currentTimeMillis();
    }

    private enum State{
        BANKEAST, BANKCLAY, SOFTEN, MINE, WAIT
    }

    private State getState(){
        if(getInventory().contains(1925) || getInventory().contains(1929)) {
            if(getInventory().contains(434)){
                log("Soften");
                return State.SOFTEN;
            }
        }

        if(!getInventory().contains(1925) || !getInventory().contains(434) || getInventory().contains(1761)){
            log("Bankeast");
            return State.BANKEAST;
        }


        if(clayCount <= clayToMine && getInventory().isFull() && !getInventory().isEmptyExcept(434) && mine){
            log("BankClay");
            return State.BANKCLAY;
        }

        if(clayCount <= clayToMine && mine){
            log("Mine");
            return State.MINE;
        }

        log("Wait");
        return State.WAIT;
    }

    @Override
    public int onLoop() throws InterruptedException{

        if(clayCount <= 0 || mine){
            log("out of clay, stopping");
            stop(false);
        }

        runTime = System.currentTimeMillis() - startTime;

        switch(getState()){
            case SOFTEN:
                softenClay();
                break;
            case BANKEAST:
                bankEast();
                break;
        }

        return random(500, 5000);
    }

    @Override
    public void onExit() {
        log("soft clay: " + softCount);
    }

    @Override
    public void onPaint(Graphics2D g){
        g.setColor(Color.WHITE);
        g.fillRect(10, 345, 300, 110);

        g.setColor(Color.BLACK);
        g.drawString("clay: " + clayCount, 20, 350);
        g.drawString("Soft clay: " + softCount, 20, 365);
        g.drawString("Time running: " + Timing.formatTime(runTime), 20, 380);

    }

    private void softenClay() throws InterruptedException{

        if(!clayArea.contains(myPosition())){
            log("Walking to clay area");
            getWalking().webWalk(clayArea);
            return;
        }

        if(!getInventory().contains(1929) && !getInventory().contains(1925)){
            log("Missing required Items");
            return;
        }

        if(!getInventory().contains(1929)){
            log("inventory does not contain water buckets");
            getInventory().interact("use", 1925);
            RS2Object fountain = getObjects().closest("fountain");
            fountain.interact("use");

            Timing.waitCondition(() -> !getInventory().contains(1925), 10000);

            return;
        }

        if(getInventory().contains(434)){
            log("inventory contains clay");

            sleep(random(500, 1000));

            if(getWidgets().get(270, 12) != null){
                RS2Widget allWidget = getWidgets().get(270, 12);
                if(!allWidget.getMessage().equals("<col=ffffff>All</col>")){
                    allWidget.interact();
                }

                RS2Widget widget = getWidgets().get(270, 14);
                widget.interact();
                Timing.waitCondition(() -> !getInventory().contains(434), 600,  15000);
            } else {
                getInventory().interact("use", 1929);
                getInventory().interact("use", 434);
            }

        }

    }

    private void bankEast() throws InterruptedException{
        if(!Banks.VARROCK_EAST.contains(myPosition())){
            getWalking().webWalk(Banks.VARROCK_EAST);
        } else if(!getBank().isOpen()){
            getBank().open();
        } else
            if(!getInventory().contains(434)){
                clayCount = getBank().getAmount(434);
                getBank().withdraw(434, 14);
                log("clay in bank: " + clayCount);
                return;
            }
            if(getInventory().contains(1761)){
                getBank().depositAllExcept(1925, 1929);
                softCount = getBank().getAmount(1761);
                log("Softclay in bank: " + softCount);
                return;
            }
    }

    private void bankWest() throws InterruptedException{
        if(!Banks.VARROCK_WEST.contains(myPosition())){
            getWalking().webWalk(Banks.VARROCK_WEST);
            return;
        }
        if(!getBank().isOpen()){
            getBank().open();
            return;
        }
        if(!getInventory().contains("clay")){
            getBank().depositAll("clay");
            return;
        }
    }

    private void mineClay() throws InterruptedException{

        //user is not in the area
        if(!mineArea.contains(myPosition())){
            getWalking().webWalk(mineArea);
            return;
        }

        Entity rock = getObjects().closest("Clay Rock");


    }






}
