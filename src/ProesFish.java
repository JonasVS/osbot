import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.script.Script;

import org.osbot.rs07.script.ScriptManifest;
import tools.Timing;

import java.awt.*;

@ScriptManifest(name = "ProesFish", author = "Proeskoet", version = 1.0, info = "Beta", logo = "")

public class ProesFish extends Script {

    //Fishing Area
    Area fishingArea = new Area(3250, 3159, 3237, 3140);
    Area walkArea = new Area(3238, 3148, 3241, 3152);

    String interactName = "net";

    @Override

    public void onStart() {

        //Code here will execute before the loop is started

    }

    private enum State {
        WALK, FISH, BANK, WAIT
    }

    private State getState(){
        //players inventory is full or there is no fishing net in the inventory
        if(getInventory().isFull()){
            return State.BANK;
        }

        //player is not in the area
        if(!fishingArea.contains(myPosition())) {
            return State.WALK;
        }

        //players inventory is not full and contains a fishing net and is in the area
        if(getInventory().contains(303) && !getInventory().isFull()){
            return State.FISH;
        }

        return State.WAIT;
    }



    @Override

    public void onExit() {

        //Code here will execute after the script ends

    }

    @Override

    public int onLoop() throws InterruptedException {

        State currentState = getState();

        if (currentState == State.WALK){
            log("Walking");
            getWalking().webWalk(walkArea);
        }

        if (currentState == State.BANK){
            log("Banking");
            if(!Banks.LUMBRIDGE_UPPER.contains(myPosition())){
                log("Walking to bank");
                getWalking().webWalk(Banks.LUMBRIDGE_UPPER);
            } else if(!getBank().isOpen()){
                log("Opening bank");
                getBank().open();
            } else if(!getInventory().isEmptyExcept(303)){
                log("Depositing in bank");
                getBank().depositAllExcept(303);
            }
        }

        if (currentState == State.FISH){
            log("fishing");

            long lastCheckedTime = 0;

            if(myPlayer().isAnimating()){
                lastCheckedTime = System.currentTimeMillis();
            } else {
                if (System.currentTimeMillis() - lastCheckedTime > 500){
                    Entity fishingSpot = getNpcs().closest(1530);
                    if(fishingSpot != null && fishingSpot.interact(interactName)){
                        Timing.waitCondition(() -> !myPlayer().isAnimating() || !fishingSpot.exists(), 5000);
                    }
                }
            }

            if(getTabs().getOpen() == Tab.INVENTORY && random(1, 100) == 20){
                getTabs().open(Tab.SKILLS);
                skills.hoverSkill(Skill.FISHING);
                sleep(random(500, 5000));
                getTabs().open(Tab.INVENTORY);
            }

        }

        if (currentState == State.WAIT) {
            log("waiting");
            sleep(5000);
        }

        return random(500, 5000); //The amount of time in milliseconds before the loop starts over

    }

    @Override

    public void onPaint(Graphics2D g) {

        //This is where you will put your code for paint(s)

    }

}
