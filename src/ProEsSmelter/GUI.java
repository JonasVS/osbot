package ProEsSmelter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class GUI {
    private final JFrame jFrame;
    private String selectedBar;
    private String selectedLocation;

    public GUI(){
        jFrame = new JFrame("ProEsSmelter");
        jFrame.setSize(500, 500);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
        mainPanel.setBorder(new EmptyBorder(20, 20, 20, 20));

        JPanel settingsPanel = new JPanel();
        settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
        TitledBorder settingsBorder = BorderFactory.createTitledBorder("Settings");
        settingsPanel.setBorder(settingsBorder);
        settingsPanel.setBounds(5, 5, 500, 250);
        mainPanel.add(settingsPanel);

        JLabel barLabel = new JLabel();
        barLabel.setText("Select a bar to smelt");
        barLabel.setBounds(10, 40, 95, 20);
        settingsPanel.add(barLabel);

        JComboBox<String> barSelect = new JComboBox<>(new String[] { "Iron bar", "dank memes"});
        barSelect.setBounds(160, 40, 95, 20);
        barSelect.addActionListener(e -> selectedBar = (String) barSelect.getSelectedItem());
        settingsPanel.add(barSelect);

        JLabel locationLabel = new JLabel();
        barLabel.setText("Select a location");
        barLabel.setBounds(10, 60, 95, 20);
        settingsPanel.add(locationLabel);

        JComboBox<String> locationSelect = new JComboBox<>(new String[] { "Falador"});
        barSelect.setBounds(160, 40, 95, 20);
        barSelect.addActionListener(e -> selectedLocation = (String) locationSelect.getSelectedItem());
        settingsPanel.add(locationSelect);

        jFrame.getContentPane().add(mainPanel);

        jFrame.setVisible(true);
    }

    public String getSelectedBar(){
        return selectedBar;
    }

    public String getSelectedLocation(){
        return selectedLocation;
    }

    public boolean isVisible(){
        return jFrame.isVisible();
    }

}
