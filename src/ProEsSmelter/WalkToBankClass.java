package ProEsSmelter;

import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.script.Script;
import tools.Task;

public class WalkToBankClass extends Task {
    public WalkToBankClass(Script script) {
        super(script);
    }

    @Override
    public int execute() {
        script.getWalking().webWalk(Banks.FALADOR_WEST);
        return 200;
    }

    @Override
    public boolean verify() {
        return script.getInventory().onlyContains("Iron bar") && !Banks.FALADOR_WEST.contains(script.myPlayer());
    }

    @Override
    public String description() {
        return "walking to bank";
    }
}
