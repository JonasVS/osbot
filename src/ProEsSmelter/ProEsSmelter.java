package ProEsSmelter;

import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;
import tools.Task;

import java.awt.*;
import java.util.ArrayList;

@ScriptManifest(name = "ProEsSmelt", author = "ProEs", version = 1.0, info = "In developement", logo = "")
public class ProEsSmelter extends Script {

    private long startTime;
    private ArrayList<Task> tasks = new ArrayList<>();
    public String outputItem = "Iron bar";
    public String currentTask = "Starting script";
    public String location = "Falador";
    public ArrayList<String> inputItems = new ArrayList<>();


    @Override
    public void onStart() throws InterruptedException {
        startTime = System.currentTimeMillis();

        GUI gui = new GUI();

        while (gui.isVisible()) {
            sleep(100);
        }

        outputItem = gui.getSelectedBar();
        location = gui.getSelectedLocation();

        log("Starting script");
        log("Selected options:");
        log("Item: " + outputItem + " location: " + location);

        tasks.add(new WalkToBankClass(this));
        tasks.add(new BankTask(this));
        tasks.add(new WalkToFurnaceTask(this));
        tasks.add(new SmeltTask(this));

        startTime = System.currentTimeMillis();
    }

    @Override
    public void onPaint(Graphics2D g) {
        long timePassed = System.currentTimeMillis() - startTime;
        int seconds = (int) (timePassed / 1000) % 60;
        int minutes = (int) ((timePassed / (1000 * 60)) % 60);
        int hours = (int) ((timePassed / (1000 * 60 * 60)));
        if (hours > 99) {
            g.drawString((hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" +
                    (seconds < 10 ? "0" : "") + seconds, 120, 440);
        } else {
            g.drawString((hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" +
                    (seconds < 10 ? "0" : "") + seconds, 120, 440);
        }
        g.drawString("Current task: " + currentTask, 120, 450);
        g.drawString("Bar: " + outputItem, 120, 460);
        g.drawString("Location: " + outputItem, 120, 470);
    }

    @Override
    public int onLoop() throws InterruptedException {
        for(Task task : tasks){
            if(task.verify()){
                currentTask = task.description();
                task.execute();
            }
        }
        return 150;
    }


}
