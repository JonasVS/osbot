package ProEsSmelter;

import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.script.Script;
import tools.Task;

public class WalkToFurnaceTask extends Task {
    Area furnace = new Area(2970, 3371, 2975, 3368);

    public WalkToFurnaceTask(Script script) {
        super(script);
    }

    @Override
    public int execute() {
        script.getWalking().webWalk(furnace);
        return 200;
    }

    @Override
    public boolean verify() {
        return script.getInventory().onlyContains("Iron ore") && !furnace.contains(script.myPlayer());
    }

    @Override
    public String description() {
        return "walking to furnace";
    }
}
