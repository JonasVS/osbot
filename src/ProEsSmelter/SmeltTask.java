package ProEsSmelter;

import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.ui.RS2Widget;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.utility.ConditionalSleep;
import tools.Task;
import tools.Timing;

public class SmeltTask extends Task {
    Area smeltArea = new Area(2970, 3371, 2975, 3368);

    public SmeltTask(Script script) {
        super(script);
    }

    @Override
    public int execute() {
        RS2Widget oreWidget = script.getWidgets().get(270, 15);
        RS2Widget allWidget = script.getWidgets().get(270, 12);

        if(oreWidget != null){
            if(!allWidget.getMessage().equals("<col=ffffff>All</col>")){
                allWidget.interact();
            }
            oreWidget.interact();
            Timing.waitCondition(() -> !script.getInventory().contains("Iron ore"), 600,  100000);
        } else {
            Entity furnace = script.getObjects().closest("Furnace");
            furnace.interact("Smelt");
        }

        return 1200;
    }

    @Override
    public boolean verify() {
        return  script.inventory.contains("Iron ore") && smeltArea.contains(script.myPlayer()) && !script.myPlayer().isAnimating();
    }

    @Override
    public String description() {
        return "smelting";
    }

    private RS2Widget getSmeltWidget(String output){
        switch(output){
            case "Bronze bar":
                return script.getWidgets().get(270, 14);
            case "Iron bar":
                return script.getWidgets().get(270, 15);
            case "Silver bar":
                return script.getWidgets().get(270, 16);
            case "Steel bar":
                return script.getWidgets().get(270, 17);
            case "Gold bar":
                return script.getWidgets().get(270, 18);
            case "Mithril bar":
                return script.getWidgets().get(270, 19);
            case "Adamite bar":
                return script.getWidgets().get(270, 20);
            case "Rune bar":
                return script.getWidgets().get(270, 21);
            default:
                return script.getWidgets().get(270, 15);
        }
    }
}
