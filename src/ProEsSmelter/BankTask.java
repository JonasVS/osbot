package ProEsSmelter;

import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.script.Script;
import tools.Task;

public class BankTask extends Task {
    public BankTask(Script script) {
        super(script);
    }

    @Override
    public int execute(){
        if (script.getBank().isOpen()) {
            if (script.getInventory().isEmpty()) {
                script.getBank().withdrawAll("Iron ore");
            } else {
                script.getBank().depositAll();
            }
        } else {
            try {
                if (!script.getBank().isOpen()) {
                    script.getBank().open();
                } else {
                    return 1200;
                }
            } catch (InterruptedException e){
                script.log(e);
            }
        }

        return 300;
    }

    @Override
    public boolean verify() {
        return (script.getInventory().isEmpty() || !script.getInventory().contains("Iron ore")) && Banks.FALADOR_WEST.contains(script.myPlayer());

    }

    @Override
    public String description() {
        return "Banking";
    }
}
